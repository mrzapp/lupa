"""
This is the main engine that runs the playback, recording and timing as a JACK client
"""

import os
import threading
import time
import json
from enum import Enum

import jack
import numpy

from .config import cfg
from .context import ctx
from .loop import LupaLoop
from .buses import LupaMetronomeBus, LupaLoopBus
from .ui.window import window

class LupaEngineRole(Enum):
    NONE = 0
    LEAD = 1
    FOLLOW = 2


class LupaEngine:
    client = None
    event = None
    update_frequency = 0.01
    role = LupaEngineRole.NONE
    buses = []
    event = None

    def init(self):
        """ Intial setup """
        
        # Initialise temp dir
        if not os.path.exists('/var/tmp/lupa'):
            os.mkdir('/var/tmp/lupa')

        # Initialise loops
        for i in range(1, 10):
            ctx.loops.append(LupaLoop(i))

        # Initialise UI
        window.init()

        # Configure JACK client
        self.client = jack.Client('Lupa')
        self.client.set_process_callback(self.on_tick)
        self.client.set_shutdown_callback(self.on_shutdown)
        self.client.set_blocksize_callback(self.on_block_size_change)

        if cfg.get('lupa', 'transport', fallback = 'none') == 'lead':
            self.role = LupaEngineRole.LEAD
            self.client.set_timebase_callback(self.timebase)

        elif cfg.get('lupa', 'transport', fallback = 'none') == 'follow':
            self.role = LupaEngineRole.FOLLOW

        # Initialise context
        ctx.block_size = self.client.blocksize
        ctx.input_delay = cfg.getint('lupa', 'input_delay', fallback = 0)
        ctx.input_data = numpy.empty((0, 2), dtype = 'float32')
        ctx.sample_rate = self.client.samplerate

        # Create input ports
        self.client.inports.register('in_1')
        self.client.inports.register('in_2')

        # Create bus output ports
        buses = list(filter(None, cfg.get('lupa', 'buses', fallback = '').split('\n')))

        if len(buses) < 1:
            out_1 = self.client.outports.register('out_1')
            out_2 = self.client.outports.register('out_2')
            loops = (l.number for l in ctx.loops)
            
            self.buses.append(LupaLoopBus(out_1, out_2, loops))

        else:
            for i, bus in enumerate(buses):
                if not bus:
                    continue

                bus = bus.strip().split('|')

                # Name assigned
                if len(bus) == 2:
                    name = bus[0]
                    loops = list(map(int, bus[1].split(',')))

                # No name assigned
                elif len(bus) == 1:
                    name = 'bus_' + str(i + 1)
                    loops = list(map(int, bus[0].split(',')))
                
                self.buses.append(LupaLoopBus(
                    self.client.outports.register(name + '_out_1'),
                    self.client.outports.register(name + '_out_2'),
                    loops
                ))

        # Create metronome bus 
        self.buses.append(LupaMetronomeBus(
            self.client.outports.register('_metronome_out_1'),
            self.client.outports.register('_metronome_out_2')
        ))
            
        # Start the client
        self.client.activate()
        
        # Connect capture ports: We expect mono input, so connect them to both inputs
        capture = self.client.get_ports(is_physical = True, is_audio = True, is_midi = False, is_output = True)
        
        if not capture:
            raise RuntimeError('No physical capture ports')

        for c in range(0, len(self.client.inports)):
            if c >= len(capture):
                break

            for i in range(0, len(self.client.inports)):
                try:
                    self.client.connect(capture[c], self.client.inports[i])
                
                except:
                    print('Could not connect capture port ', src)

        # Connect all output ports to the first 2 input ports we can find
        playback = self.client.get_ports(is_physical = True, is_audio = True, is_midi = False, is_input = True)
        
        if not playback or len(playback) < 2:
            raise RuntimeError('No physical playback ports')

        for p in range(0, len(self.client.outports), 2):
            for i in range(0, 2):
                src = self.client.outports[p + i]
                dest = playback[i]

                try:
                    self.client.connect(src, dest)
                
                except:
                    print('Could not connect playback port ', src)

        # Load existing session
        self.load_session()

        # Block exit
        self.event = threading.Event()

        try:
            self.event.wait()
        
        except KeyboardInterrupt:
            ctx.is_running = False

    def timebase(self, state, blocksize, pos, new_pos):
        """ Timebase callback for when the engine is leading """

        pos.frame = ctx.beat_playback_position + (ctx.beats_since_last_cycle * ctx.get_frames_per_beat())
        pos.beat = ctx.beats_since_last_cycle + 1
        pos.beats_per_bar = ctx.beats_per_cycle
        pos.beats_per_minute = ctx.beats_per_minute
        pos.bar = 1

    def load_session(self):
        """ Loads the existing session, if any """
        
        if not os.path.exists('/var/tmp/lupa/session.json'):
            return
        
        try:
            file = open('/var/tmp/lupa/session.json', 'r')
            session = json.load(file)
            file.close()

        except Exception:
            print('ERROR: Could not load session file')
            return

        if 'beats_per_cycle' in session:
            ctx.beats_per_cycle = session['beats_per_cycle']

        if 'beats_per_minute' in session:
            ctx.beats_per_minute = session['beats_per_minute']

        if 'loops' in session:
            for i, loop in enumerate(session['loops']):
                if i >= len(ctx.loops):
                    break

                if 'offset' in loop:
                    ctx.loops[i].offset = loop['offset']

                if 'volume' in loop:
                    ctx.loops[i].volume = loop['volume']
                
                if 'is_reversed' in loop:
                    ctx.loops[i].is_reversed = loop['is_reversed']
                
                if 'speed' in loop:
                    ctx.loops[i].speed = max(0.1, loop['speed'])

                if 'sequence' in loop:
                    ctx.loops[i].sequence = numpy.array(loop['sequence'], dtype = 'bool')

    def save_session(self):
        """ Saves the in-memory session, if any """
        
        session = {
            'beats_per_cycle': ctx.beats_per_cycle,
            'beats_per_minute': ctx.beats_per_minute,
            'loops': []
        }

        for loop in ctx.loops:
            session['loops'].append({
                'offset': loop.offset,
                'volume': loop.volume,
                'speed': max(0.1, loop.speed),
                'is_reversed': loop.is_reversed,
                'sequence': loop.sequence.tolist()
            })

        if not os.path.exists('/var/tmp/lupa'):
            os.mkdir('/var/tmp/lupa')

        with open('/var/tmp/lupa/session.json', 'w') as file:
            json.dump(session, file, indent = 4)

    def on_block_size_change(self, blocksize):
        """ Called when JACK changes this client's block size """

        ctx.block_size = blocksize

    def on_tick(self, frames):
        """ The processing event """
        
        assert frames == self.client.blocksize
        
        # Quit
        if not ctx.is_running:
            self.save_session()
            self.event.set()
            self.client.deactivate()
            return

        # Update UI
        window.tick()
            
        # Check JACK transport
        state, position = self.client.transport_query()

        if self.role == LupaEngineRole.FOLLOW:
            ctx.layout.play_button.is_active = state == jack.ROLLING

        elif self.role == LupaEngineRole.LEAD:
            if ctx.is_playing() and self.client.transport_state != jack.ROLLING:
                self.client.transport_stop()

            elif not ctx.is_playing() and self.client.transport_state != jack.STOPPED:
                self.client.transport_stop()

        # Blink (for buttons with blinking lights)
        ctx.blink_timer += frames

        if ctx.blink_timer >= ctx.get_frames_per_beat() / 2:
            ctx.blink_state = not ctx.blink_state
            ctx.blink_timer = 0

        # Recording
        if ctx.is_recording():
            input_block = numpy.column_stack((self.client.inports[0].get_array(), self.client.inports[1].get_array()))

            ctx.input_data = numpy.append(ctx.input_data, input_block, 0)
           
        # Playback
        if ctx.is_playing():
            # Buses
            for bus in self.buses:
                bus.play()

            # Beat clock
            if self.role == LupaEngineRole.FOLLOW:
                beat = position['beat'] - 1
                
                ctx.beat_playback_position = int(position['frame'] - ((position['beat'] - 1) + (position['bar'] - 1) * position['beats_per_bar']) * ctx.get_frames_per_beat())
                ctx.beats_per_minute = int(position['beats_per_minute'])
                ctx.beats_per_cycle = int(position['beats_per_bar'])
                ctx.is_beat = beat != ctx.beats_since_last_cycle
                ctx.beats_since_last_cycle = beat
                ctx.is_cycle = beat == 0

            else:
                ctx.beat_playback_position += frames

                # This is a beat
                if ctx.beat_playback_position >= ctx.get_frames_per_beat():
                    ctx.is_beat = True
                    ctx.beats_since_last_cycle += 1
                    ctx.beat_playback_position -= ctx.get_frames_per_beat()
                    ctx.beat_playback_delay = ctx.beat_playback_position

                    # This is a cycle
                    if ctx.is_repeating() or ctx.beats_since_last_cycle >= ctx.beats_per_cycle:
                        ctx.is_cycle = True
                        ctx.beats_since_last_cycle = 0
                        ctx.cycle_playback_delay = ctx.beat_playback_delay

                else:
                    ctx.is_beat = False
                    ctx.is_cycle = False

                if self.role == LupaEngineRole.LEAD:
                    self.client.transport_frame = ctx.beat_playback_position + (ctx.beats_since_last_cycle * ctx.get_frames_per_beat())
        
        # Update loops, avoiding sudden escalation of CPU usage by spacing them out
        for loop in ctx.loops:
            if loop.is_dirty and time.time() - loop.last_update > 0.1:
                loop.update_async()
                break
        
    def on_shutdown(self, status, reason):
        """ JACK shutdown event """

        print('JACK shutdown!')
        print('    Status:', status)
        print('    Reason:', reason)

engine = LupaEngine()

import os
import re

import markdown as md

app_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Load the markdown content
readme_markdown = open(os.path.join(app_dir, 'README.md'), 'r').read()

# Extract the version number from the __main__.py file
version = re.search(r"__version__ = '([0-9.]+)'", open(os.path.join(app_dir, '__main__.py'), 'r').read()).groups()[0]

# Insert icons in place of button names
icons = {
    'METRONOME': 'metronome',
    'UP': 'caret-up',
    'DOWN': 'caret-down',
    'TEMPO': 'clock',
    'CYCLE': 'rotate-right',
    'SEQUENCER': 'sequencer',
    'NUMBER': 'numbers',
    'RECORD': 'microphone',
    'PLAYBACK': 'play'
}

for name, svg in icons.items():
    readme_markdown = readme_markdown.replace('`' + name + '`', '![' + name + '](assets/images/' + svg + '.svg)')

# Create heading anchors and table of contents
headings = {}
toc = ''

def replace_headings(match):
    global headings

    text = match.group(1)
    slug = text.lower().replace(' ', '-')

    headings[text] = slug

    return '## <a href="#' + slug + '" id="' + slug + '">' + text + '</a>'

readme_markdown = re.sub(r'\n## ([A-z ]+)', replace_headings, readme_markdown)

for text in headings:
    if text == 'Contents':
        continue

    slug = headings[text]

    toc += '  \n- [' + text + '](#' + slug + ')'

readme_markdown = readme_markdown.replace('[TOC]', toc)

# Convert markdown to HTML
readme_html = md.markdown(readme_markdown, extensions = [ 'extra' ]).replace('binary release', 'binary release (' + version + ')')

title = 'Lupa - An audio looper and sequencer for Linux'
description = 'Lupa is a simple open-source audio looper and step sequencer for Linux, built for monkeying around with music'

html = '''
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="''' + description + '''">
        <meta property="og:title" content="'''  + title + '''" />
        <meta property="og:description" content="''' + description + '''" />
        <meta property="og:image" content="https://mrzapp.gitlab.io/lupa/screenshot.jpg" />
        <title>''' + title + '''</title>
        <link rel="icon" href="./icon.svg">
        <style type="text/css">
            @font-face {
                font-family: 'Cantarell';
                src: url('./assets/fonts/cantarell.otf');
            }
            html {
                background: url('./assets/images/background.svg');
                background-position: center;
                background-size: cover;
                background-attachment: fixed;
                background-color: rgba(30, 30, 30);
            }
            body {
                font-family: 'Cantarell', sans-serif;
                margin: 40px auto;
                max-width: 44rem;
                line-height: 1.6;
                font-size: 18px;
                color: rgba(189, 189, 189);
                padding: 0 10px
            }
            a {
                color: white;
            }
            h1 {
                display: none;
            }
            h2 a {
                color: inherit;
            }
            h1, h2, h3 {
                line-height: 1.2;
                margin: 1.6em 0 0.5em 0;
            }
            h2 {
                text-decoration: underline;
            }
            table thead {
                text-align: left;
            }
            table tr td {
                vertical-align: top;
            }
            table tr td:first-of-type {
                white-space: nowrap;
                padding-right: 2rem;
            }
            img[alt="Lupa logo"] {
                display: block;
                height: 10rem;
                margin: 0 auto;
            }
            img[alt="Screenshot"] {
                display: block;
                margin: 1rem auto;
                width: 100%;
                box-shadow: 0 0 1rem rgba(0, 0, 0, 0.5);
            }
            ''' + 'img[alt="' + '"], img[alt="'.join(icons.keys()) + '"]' + ''' {
                display: inline-block;
                width: 1rem;
                padding: 0.2rem;
                background-color: rgba(48, 48, 48);
                border-radius: 0.2rem;
                margin: 0 0.5rem;
                position: relative;
                top: 0.3rem;
            }
            code {
                color: white;
            }
        </style>
    </head>
    <body>
        ''' + readme_html + '''
    </body>
</html>
'''

if not os.path.exists(os.path.join(app_dir, 'public')):
    os.mkdir(os.path.join(app_dir, 'public'))

file = open(os.path.join(app_dir, 'public', 'index.html'), 'w')
file.write(html)
file.close()

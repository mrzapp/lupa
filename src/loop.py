"""
The abstract audio loop
"""

import os
import time
import math
import threading
from enum import Enum

import soundfile
import numpy

from .context import ctx

class LupaLoopState(Enum):
    STOPPED = 0
    PLAYING = 1
    RECORDING = 2

class LupaLoop:
    current_state = LupaLoopState.STOPPED
    is_dirty = True
    is_reversed = False
    last_changed_value = ''
    last_update = 0
    number = 0
    offset = 0
    queued_state = LupaLoopState.STOPPED
    recording = numpy.empty((0, 2), dtype = 'float32')     # The original, unaltered recording
    sequence = None
    sound = numpy.empty((0, 2), dtype = 'float32')         # The sound that's presented to the engine
    speed = 1.0
    update_trhead = None
    volume = 100

    def __init__(self, p_number):
        """ Constructor """

        self.number = p_number
        self.sequence = numpy.zeros(ctx.beats_per_cycle * 4, dtype = 'bool');
        self.set_sequence_step(0, True)

        # If a loop has already been recorded, load it
        if os.path.exists('/var/tmp/lupa/loop_' + str(self.number) + '.wav'):
            self.recording = ctx.ensure_stereo(soundfile.read('/var/tmp/lupa/loop_' + str(self.number) + '.wav', dtype = 'float32')[0])
            self.sound = self.recording.copy()

    def update_async(self):
        """ Calls the update method in a separate thread """

        self.update_thread = threading.Thread(target = self.update)
        self.update_thread.start()
        self.is_dirty = False

    def update(self):
        """ Updates this loop from a dirty state """

        # Start the modified sound from scratch
        self.sound = self.recording.copy()

        # Apply effects
        if self.sound.any():
            # Reverse
            if self.is_reversed:
                self.sound = numpy.flip(self.sound)

            # Offset
            if self.offset != 0:
                offset_frames = self.get_offset_frames()
                self.sound = numpy.roll(self.sound, (offset_frames, offset_frames))
           
            # Volume
            self.sound *= self.volume / 100

            # Speed
            if self.speed != 1:
                left, right = numpy.split(self.sound, 2, 1)
                left = left.flatten()
                right = right.flatten()

                left = numpy.interp(numpy.arange(0, len(left), self.speed), numpy.arange(0, len(left)), left).astype('float32')
                right = numpy.interp(numpy.arange(0, len(right), self.speed), numpy.arange(0, len(right)), right).astype('float32')

                if left.shape[0] < right.shape[0]:
                    right = numpy.resize(right, left.shape[0])
                
                elif left.shape[0] > right.shape[0]:
                    left = numpy.resize(left, right.shape[0])

                self.sound = numpy.column_stack((left, right))

        # Ensure the length of the sound matches our expectations (repeat if shorter)
        self.sound = ctx.ensure_length(self.sound, self.get_length(), True)

        self.last_update = time.time()
        self.is_dirty = False

    def start_playing(self):
        """ Starts playing immediately """

        if not self.sound.any():
            self.stop_playing()
        
        else:
            self.current_state = LupaLoopState.PLAYING
            self.queued_state = LupaLoopState.PLAYING
    
    def stop_playing(self):
        """ Stops playing immediately """

        self.current_state = LupaLoopState.STOPPED
        self.queued_state = LupaLoopState.STOPPED

    def toggle_play(self):
        """ Toggles the queued state between playing and stopped """

        if not self.sound.any():
            return

        if self.queued_state == LupaLoopState.PLAYING:
            self.queued_state = LupaLoopState.STOPPED
        
        elif self.queued_state == LupaLoopState.STOPPED:
            self.queued_state = LupaLoopState.PLAYING

    def is_playing(self) -> bool:
        return ctx.is_playing() and (self.current_state == LupaLoopState.PLAYING or self.current_state == LupaLoopState.RECORDING)
    
    def is_queued_for_playing(self) -> bool:
        return self.queued_state == LupaLoopState.PLAYING
    
    def is_recording(self) -> bool:
        return self.current_state == LupaLoopState.RECORDING

    def is_queued_for_recording(self) -> bool:
        return self.queued_state == LupaLoopState.RECORDING
    
    def is_queued_to_stop(self) -> bool:
        return self.queued_state == LupaLoopState.STOPPED

    def cancel_queued_state(self):
        self.queued_state = self.current_state

    def stop_other_recordings(self):
        """ Stops all other loop recordings """

        for loop in ctx.loops:
            if loop == self:
                continue

            if loop.is_recording():
                loop.stop_recording()
                loop.start_playing()

            elif loop.is_queued_for_recording():
                loop.cancel_queued_state()

    def start_recording(self):
        """ Starts recording immediately """
        
        self.stop_other_recordings()

        self.current_state = LupaLoopState.RECORDING
        self.queued_state = LupaLoopState.RECORDING
    
    def queue_recording(self):
        """ Queues recording """

        self.offset = 0
        self.stop_other_recordings()

        self.queued_state = LupaLoopState.RECORDING
    
    def stop_recording(self):
        """ Stops recording immediately """
        
        # Cache recording
        recording = ctx.input_data
        ctx.input_data = numpy.empty((0, 2), dtype = 'float32')
       
        # Apply input delay compensation: Remove bytes from the beginning and add zeros to the end
        if ctx.input_delay > 0:
            delay_samples = int((ctx.input_delay / 1000) * ctx.sample_rate)
            recording = recording[delay_samples:]
            recording = ctx.ensure_length(recording, recording.shape[0] + delay_samples)
       
        # On-tempo recording
        if ctx.is_playing():
            # Match recording length to loop
            recording = ctx.ensure_length(recording, self.get_length())
            
            # Overdub the new recording onto the existing one
            if self.recording.any():
                recording = numpy.add(ctx.ensure_length(self.recording, self.get_length()), recording)

        # Off-tempo recording
        else:
            ctx.beats_per_minute = max(20, min(200, (int(60 / ((recording.shape[0] / ctx.sample_rate) / ctx.beats_per_cycle)) // 2) * 2))
            self.start_playing()
            
            ctx.layout.play_button.is_active = True
            ctx.layout.tempo_button.show_label()

            for loop in ctx.loops:
                loop.is_dirty = True

        # Replace the existing sound with the new recording
        self.set_sound(recording)
    
    def absorb_loop_recording(self, loop):
        """ Absorbs another loop into this one, removing its recording and merging it into this one """

        if not loop or not loop.recording.any():
            return

        # Mix the recordings
        new_sound = numpy.empty((0, 2), dtype = 'float32')
        
        for pos in range(0, self.get_length(), ctx.block_size):
            this_pos = self.get_playback_position(pos)
            other_pos = loop.get_playback_position(pos)

            this_block = ctx.ensure_length(self.sound[this_pos:this_pos + ctx.block_size], ctx.block_size)
            other_block = ctx.ensure_length(loop.sound[other_pos:other_pos + ctx.block_size], ctx.block_size)

            new_sound = numpy.append(new_sound, numpy.add(this_block, other_block), 0)

        self.set_sound(ctx.ensure_length(new_sound, self.get_length()))
        
        # Reset this loop
        self.reset_all()

        # Clear the other loop
        loop.reset_all()
        loop.set_sound(numpy.empty((0, 2), dtype = 'float32'))
        loop.stop_playing()

    def clear(self):
        """ Erases this recording """

        self.set_sound(numpy.empty((0, 2), dtype = 'float32'))
        self.is_dirty = True
        self.current_state = LupaLoopState.STOPPED
        self.queued_state = LupaLoopState.STOPPED
        self.reset_all()

        if ctx.focused_loop == self:
            ctx.focused_loop = None

    def on_trigger(self):
        """ Start queued behaviour when triggered """
        
        # Finish recording
        if self.is_recording():
            self.stop_recording()

        # Start recording (overdubbing the previous recording)
        if self.queued_state == LupaLoopState.RECORDING:
            self.start_recording()

        # Start playing
        elif ctx.is_playing() and self.queued_state == LupaLoopState.PLAYING:
            self.start_playing()
        
        # Stop
        else:
            self.stop_playing()

    def reset_all(self):
        """ Resets this loop to its default state """
        
        self.is_reversed = False
        self.offset = 0
        self.reset_sequence()
        self.speed = 1
        self.volume = 100

    def reset_sequence(self):
        """ Sets the state of the sequence to its default state """

        self.sequence = numpy.zeros(ctx.beats_per_cycle * 4, dtype = 'bool');
        self.set_sequence_step(0, True)

    def set_sequence_step(self, index, state):
        """ Sets the state of a sequence step """

        length = ctx.beats_per_cycle * 4

        if self.sequence.shape[0] < length:
            self.sequence = numpy.pad(self.sequence, (0, length - self.sequence.shape[0]))

        elif self.sequence.shape[0] > length:
            self.sequence = numpy.resize(self.sequence, length)

        assert index < self.sequence.shape[0]

        self.sequence[index] = state
        self.is_dirty = True

    def get_sequence_step(self, index) -> bool:
        """ Gets the state of a sequence step """

        if index < self.sequence.shape[0]:
            return self.sequence[index]

        return False
    
    def toggle_sequence_step(self, index):
        """ Toggles the state of a sequence step on/off """

        self.set_sequence_step(index, not self.get_sequence_step(index))

    def get_playback_position(self, cycle_position = None) -> int:
        """ Gets the playback position, accounting for the step sequence """

        if cycle_position == None:
            cycle_position = ctx.get_frames_since_last_cycle()

        sequence_length = ctx.beats_per_cycle * 4
        step_length = int((ctx.get_frames_per_beat() * ctx.beats_per_cycle) / (sequence_length))
        loop_position = -1

        for i in range(0, sequence_length):
            if i >= self.sequence.shape[0] or not self.sequence[i]:
                continue
           
            # Whenever we hit an active step, reset the playback position
            step_frame = step_length * i
           
            if cycle_position >= step_frame:
                loop_position = cycle_position - step_frame

        return loop_position
    
    def change_speed(self, direction):
        """ Changes the speed """

        self.speed = max(0.1, min(4, self.speed + direction * 0.01))
        self.speed = round(self.speed, 2)

        self.last_changed_value = 'speed'
        self.is_dirty = True

    def change_offset(self, direction):
        """ Changes the offset in 10s of milliseconds """

        length = int(self.recording.shape[0] / ctx.sample_rate * 1000)

        self.offset = int(self.offset + 10 * direction)
    
        while self.offset >= length:
            self.offset -= length

        while self.offset <= -length:
            self.offset += length

        self.offset = int(self.offset // 10) * 10
        self.last_changed_value = 'offset'
        self.is_dirty = True
    
    def change_volume(self, direction):
        """ Changes the volume """

        self.volume = max(10, min(800, self.volume + direction))
        self.last_changed_value = 'volume'
        self.is_dirty = True

    def toggle_reversed(self):
        """ Toggles loop reversal on/off """

        self.is_reversed = not self.is_reversed
        self.last_changed_value = 'reversed'
        self.is_dirty = True

    def set_sound(self, sound):
        """ Sets a new sound and writes the recording to disk """
        
        self.recording = sound.copy()
        self.sound = self.recording.copy()
        
        soundfile.write('/var/tmp/lupa/loop_' + str(self.number) + '.wav', self.recording, ctx.sample_rate)
        
        self.is_dirty = True

    def get_offset_frames(self) -> int:
        if self.offset == 0:
            return 0

        return int((self.offset / 1000) * ctx.sample_rate)

    def get_length(self) -> int:
        """ Gets the desired length of the sound in frames """

        return ctx.get_frames_per_beat() * ctx.beats_per_cycle

import numpy
import soundfile

from .context import ctx

class LupaBus:
    """ The class responsible for mixing and outputting audio """

    port_1 = None
    port_2 = None
    block = None

    def __init__(self, port_1, port_2):
        """ Constructor """

        self.port_1 = port_1
        self.port_2 = port_2
        self.block = numpy.zeros((ctx.block_size, 2), dtype = 'float32')

    def mix(self):
        """ Mixes the audio signal before playback """

    def play(self):
        """ Plays back the audio signal to the output ports """
        
        self.block = numpy.zeros((ctx.block_size, 2), dtype = 'float32')

        self.mix()

        self.block = numpy.clip(-1, 1, self.block)

        # Divide data into stereo signal
        block_1, block_2 = numpy.split(self.block, 2, 1)
        block_1 = block_1.flatten()
        block_2 = block_2.flatten()

        buffer_1 = self.port_1.get_buffer()
        buffer_2 = self.port_2.get_buffer()

        buffer_1[:] = block_1
        buffer_2[:] = block_2

class LupaMetronomeBus(LupaBus):
    """ Mixes and outputs the metronome audio """

    beat_sound = None
    cycle_sound = None

    def __init__(self, port_1, port_2):
        super().__init__(port_1, port_2)

        self.beat_sound = ctx.ensure_stereo(soundfile.read(ctx.path('assets/audio/metronome_beat.ogg'), dtype = 'float32')[0])
        self.cycle_sound = ctx.ensure_stereo(soundfile.read(ctx.path('assets/audio/metronome_cycle.ogg'), dtype = 'float32')[0])

    def mix(self):
        """ Plays the "tick" on cycles and the "tock" on beats """
        if not ctx.is_metronome_active():
            return

        # Cycle
        if ctx.get_frames_since_last_cycle() + ctx.block_size < self.cycle_sound.size:
            from_frame = ctx.get_frames_since_last_cycle() - ctx.beat_playback_delay
            to_frame = from_frame + ctx.block_size
            cycle_block = self.cycle_sound[from_frame : to_frame]

            # Pad the rest of the sound with zeros if needed
            if cycle_block.shape[0] < self.block.shape[0]:
                cycle_block = numpy.pad(cycle_block, ((0, self.block.shape[0] - cycle_block.shape[0]), (0, 0)))

            self.block = cycle_block
        
        # Beat
        elif ctx.get_frames_since_last_beat() + ctx.block_size < self.beat_sound.size:
            from_frame = ctx.get_frames_since_last_beat() - ctx.beat_playback_delay
            to_frame = from_frame + ctx.block_size
            beat_block = self.beat_sound[from_frame : to_frame]

            # Pad the rest of the sound with zeros if needed
            if beat_block.shape[0] < self.block.shape[0]:
                beat_block = numpy.pad(beat_block, ((0, self.block.shape[0] - beat_block.shape[0]), (0, 0)))
            
            self.block = beat_block

class LupaLoopBus(LupaBus):
    """ Mixes and outputs loop audio """

    loops = None
    
    def __init__(self, port_1, port_2, loops):
        self.loops = []

        for number in loops:
            for loop in ctx.loops:
                if loop.number == number:
                    self.loops.append(loop)

        super().__init__(port_1, port_2)

    def mix(self):
        for loop in self.loops:
            if ctx.is_cycle:
                loop.on_trigger()

            if not loop.is_playing():
                continue

            from_frame = loop.get_playback_position() - ctx.cycle_playback_delay

            if from_frame < 0:
                continue

            to_frame = from_frame + ctx.block_size

            loop_block = loop.sound[from_frame : to_frame]
           
            # Add sound block to mix
            if loop_block.shape[0] == self.block.shape[0]:
                self.block = numpy.add(self.block, loop_block)

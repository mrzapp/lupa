import os
import time
import sys

import numpy
import pygame as pg

class Context:
    # Settings
    beats_per_cycle = 4
    beats_per_minute = 90
    block_size = 1024
    input_delay = 0
    sample_rate = 0

    # State
    beat_playback_delay = 0
    beat_playback_position = 0
    beats_since_last_cycle = 0
    blink_state = True
    blink_timer = 0
    cycle_playback_delay = 0
    input_data = None
    is_beat = True
    is_cycle = True
    is_running = True
    loops = []

    # UI
    log_string = ''
    focused_loop = None
    font = None
    last_event = None
    layout = None
    pointer_position = pg.Vector2(0, 0)
    screen = None
    select_rect = None
    window = None

    def was_last_event_pointer(self) -> bool:
        if not self.last_event:
            return False
        
        t = self.last_event.type

        return t == pg.FINGERDOWN or t == pg.MOUSEBUTTONDOWN or t == pg.FINGERUP or t == pg.MOUSEBUTTONUP
    
    def was_last_event_key(self) -> bool:
        if not self.last_event:
            return False
        
        t = self.last_event.type

        return t == pg.KEYDOWN or t == pg.KEYUP

    def get_current_step(self) -> int:
        position = self.get_frames_since_last_cycle() / self.get_frames_per_cycle()
        total_steps = self.beats_per_cycle * 4

        return int(position * total_steps)

    def set_focused_loop(self, loop):
        if self.focused_loop:
            self.focused_loop.is_dirty = True
        
        if loop:
            loop.is_dirty = True

        self.focused_loop = loop
        self.is_dirty = True

    def get_frames_per_beat(self) -> int:
        return int((60 / self.beats_per_minute) * self.sample_rate)
    
    def get_frames_per_cycle(self) -> int:
        return int(self.get_frames_per_beat() * self.beats_per_cycle)
    
    def get_frames_per_step(self) -> int:
        return int(self.get_frames_per_beat() / 4)
            
    def get_frames_since_last_beat(self) -> int:
        return self.beat_playback_position
    
    def get_frames_since_last_cycle(self) -> int:
        return self.get_frames_since_last_beat() + self.get_frames_per_beat() * self.beats_since_last_cycle

    def get_time_since_last_beat(self) -> float:
        frames = self.beat_playback_position

        if frames <= 0:
            return 0

        return frames / self.sample_rate
    
    def get_time_since_last_cycle(self) -> float:
        frames = self.get_frames_since_last_cycle()

        if frames <= 0:
            return 0

        return frames / self.sample_rate

    def is_playing(self) -> bool:
        return self.layout and self.layout.play_button.is_active
   
    def is_recording_armed(self) -> bool:
        return self.layout.record_button.is_active or self.layout.record_button.is_pressed()
    
    def get_recording_loop(self) -> object:
        """ Gets the loop currently being recorded onto """

        for loop in self.loops:
            if loop.is_recording():
                return loop

        return None
    
    def get_screen_size(self) -> pg.Vector2:
        return pg.Vector2(self.layout.screen_width, self.layout.screen_height)

    def get_loop_buttons(self) -> list:
        return list(filter(lambda item: item is not None, self.layout.loop_buttons))

    def get_action_buttons(self) -> list:
        return list(filter(lambda item: item is not None, [ 
            self.layout.record_button,
            self.layout.metronome_button,
            self.layout.tempo_button,
            self.layout.cycle_button,
            self.layout.play_button,
            self.layout.sequencer_button,
            self.layout.increase_button,
            self.layout.decrease_button
        ]))

    def get_all_buttons(self) -> list:
        return self.get_loop_buttons() + self.get_action_buttons()

    def is_recording(self) -> bool:
        return self.get_recording_loop() != None

    def is_repeating(self) -> bool:
        return self.layout and self.layout.play_button.is_held_down()

    def is_sequence_mode(self) -> bool:
        return self.layout and self.layout.sequencer_button.is_active

    def is_metronome_active(self) -> bool:
        return self.layout.metronome_button.is_active

    def ensure_stereo(self, audio) -> object:
        """ Ensures that an audio file is in stereo """

        if audio.ndim == 1:
            return numpy.repeat(audio, 2).reshape(audio.size, 2)

        return audio

    def ensure_length(self, audio, length, wrap = False) -> object:
        """ Ensures that an audio file has a certain length """

        new_audio = audio.copy()

        # Pad recording to match length
        if new_audio.shape[0] < length:
            diff = length - audio.shape[0]

            if wrap and audio.any():
                while new_audio.shape[0] < length:
                    new_audio = numpy.append(new_audio, audio[:diff], 0)
            
            else:
                new_audio = numpy.pad(audio, ((0, diff), (0, 0)))

        # Truncate recording to match length
        if new_audio.shape[0] > length:
            new_audio = numpy.resize(new_audio, (length, 2))

        return new_audio

    def log(self, *args):
        self.log_string = ', '.join(map(str,args))

    def path(self, path) -> str:
        """ Transforms a relative path to an absolute one dependent on the "frozen" flag """

        if getattr(sys, 'frozen', False):
            app_dir = sys._MEIPASS
        
        else:
            app_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        
        resolved_path = os.path.abspath(os.path.join(app_dir, path))

        return resolved_path

ctx = Context()

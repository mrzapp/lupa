import os
import configparser
import pathlib
import shutil

from .context import ctx

cfg = configparser.ConfigParser()

config_directory_path = os.path.join(pathlib.Path.home(), '.config', 'lupa')
config_file_path = os.path.join(config_directory_path, 'lupa.cfg')

if not os.path.exists(config_directory_path):
    os.mkdir(config_directory_path)

if not os.path.exists(config_file_path):
    shutil.copyfile(ctx.path('lupa.cfg'), config_file_path)    

cfg.read(config_file_path)

if not 'lupa' in cfg or not 'layout' in cfg['lupa']:
    shutil.copyfile(ctx.path('lupa.cfg'), config_file_path)    

    cfg.read(config_file_path)

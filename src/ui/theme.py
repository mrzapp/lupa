class Theme:
    background = None
    button_default = None
    button_active = None
    button_alert = None
    button_text = None
    button_selected = None

    def __init__(self, **kwargs):
        for k in kwargs:
            setattr(self, k, kwargs[k])

theme = Theme(
    background = (30, 30, 30),
    button_default = (48, 48, 48),
    button_alert = (192, 28, 40),
    button_active = (21, 83, 158),
    button_text = (255, 255, 255),
    button_selected = (154, 153, 150),
)

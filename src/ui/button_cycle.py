from .buttons import LupaButton
from .theme import theme
from ..context import ctx

class LupaCycleButton(LupaButton):
    """ The button for changing beats per cycle """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'rotate-right')
    
    def get_label(self) -> str:
        return str(ctx.beats_per_cycle) + ' beats'

    def draw(self):
        """ Draws the cycle button, highlighting it every cycle """

        color = theme.button_default

        if (ctx.is_playing() and ctx.get_time_since_last_cycle() < 0.1) or self.is_pressed():
            color = theme.button_active

        super().draw(color)
    
    def on_slide_y(self, y, dy, my):
        """ Pointer slid across this button vertically: Change cycle """
        
        (bx, by, bw, bh) = self.get_screen_rect()

        if my % int(bh / 4) == 0:
            self.change_cycle(-dy)

    def change_cycle(self, direction):
        """ Change the beats per cycle """

        previous_beats_per_cycle = ctx.beats_per_cycle

        ctx.beats_per_cycle += direction

        ctx.beats_per_cycle = int(max(2, min(16, ctx.beats_per_cycle)))
        self.show_label()

        # Update loops
        for loop in ctx.loops:
            loop.is_dirty = True

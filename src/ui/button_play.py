from .buttons import LupaToggleButton
from ..context import ctx

class LupaPlayButton(LupaToggleButton):
    """ The button for toggling playback """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'play')

    def on_tapped(self):
        """ Play button tapped """

        # We want to flip a loop
        for button in ctx.get_loop_buttons():
            if button.is_pressed():
                button.loop.toggle_reversed()
                return

        super().on_tapped()

import pygame as pg

from .button_metronome import LupaMetronomeButton
from .button_tempo import LupaTempoButton
from .button_cycle import LupaCycleButton
from .button_sequencer import LupaSequencerButton
from .button_record import LupaRecordButton
from .button_play import LupaPlayButton
from .button_increase import LupaIncreaseButton
from .button_decrease import LupaDecreaseButton
from .button_loop import LupaLoopButton
from ..context import ctx

class LupaLayout:
    screen_width = 370
    screen_height = 460
    screen_grid_width = 10
    screen_grid_height = 10
    screen_margin_size = 10
    metronome_button = None
    tempo_button = None
    cycle_button = None
    sequencer_button = None
    record_button = None
    play_button = None
    increase_button = None
    decrease_button = None
    loop_buttons = []

    def __init__(self, config):
        self.screen_width = config.getint('screen_width', fallback = 4)
        self.screen_height = config.getint('screen_height', fallback = 5)
        self.screen_grid_width = config.getint('screen_grid_width', fallback = 4)
        self.screen_grid_height = config.getint('screen_grid_height', fallback = 5)
        self.screen_margin_size = config.getint('screen_margin_size', fallback = 10)
        self.metronome_button = self.create_action_button(LupaMetronomeButton, config.get('metronome_button'))
        self.tempo_button = self.create_action_button(LupaTempoButton, config.get('tempo_button'))
        self.cycle_button = self.create_action_button(LupaCycleButton, config.get('cycle_button'))
        self.sequencer_button = self.create_action_button(LupaSequencerButton, config.get('sequencer_button'))
        self.record_button = self.create_action_button(LupaRecordButton, config.get('record_button'))
        self.play_button = self.create_action_button(LupaPlayButton, config.get('play_button'))
        self.increase_button = self.create_action_button(LupaIncreaseButton, config.get('increase_button'))
        self.decrease_button = self.create_action_button(LupaDecreaseButton, config.get('decrease_button'))

        loop_number = 0

        for config in config.get('loop_buttons', fallback = '').split('\n'):
            config = config.strip()

            if not config:
                continue

            loop_number += 1
            self.loop_buttons.append(self.create_loop_button(loop_number, config))

    def create_action_button(self, constructor, config) -> object:
        if not config:
            return None

        config_strings = config.split('|')

        # Key assigned
        if len(config_strings) == 2:
            key = pg.key.key_code(config_strings[0])
            rect = list(map(float, config_strings[1].split(',')))

        # No key assigned
        elif len(config_strings) == 1:
            key = -1
            rect = list(map(float, config_strings[0].split(',')))

        return constructor(key, rect)

    def create_loop_button(self, number, config) -> object:
        if not config:
            return None

        config_strings = config.split('|')

        # Key assigned
        if len(config_strings) == 2:
            key = pg.key.key_code(config_strings[0])
            rect = list(map(float, config_strings[1].split(',')))

        # No key assigned
        elif len(config_strings) == 1:
            key = -1
            rect = list(map(float, config_strings[0].split(',')))
        
        return LupaLoopButton(key, rect, number)

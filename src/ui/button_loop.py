import os
import time
import math

import pygame as pg

import numpy

from .buttons import LupaButton
from .theme import theme
from ..context import ctx

class LupaLoopButton(LupaButton):
    """ The button responsible for recording and playing loops """

    loop = None
    sequence = None
    last_loop_update = 0

    def __init__(self, p_key, p_rect, p_number):
        """ Constructor """

        super().__init__(p_key, p_rect)

        for loop in ctx.loops:
            if loop.number == p_number:
                self.loop = loop
                break
    
    def toggle_sequence_step(self):
        """ Toggle a sequence step on/off """

        if not self.loop.sound.any():
            return
        
        sequence_length = ctx.beats_per_cycle * 4
        step_length = int((ctx.get_frames_per_beat() * ctx.beats_per_cycle) / (sequence_length))
        step_index = -1

        # Based on the position of the step indicator
        if ctx.was_last_event_pointer():
            (x, y, w, h) = self.get_window_rect()
            sequence_center = (x + w / 2, y + h / 2)
            step_size = w * 0.9 / min(64, max(16, sequence_length))
            closest = 100000

            for i in range(0, sequence_length):
                step_frame = i * step_length 
                step_radius_outer = w * 0.3

                t = (i / sequence_length) * math.pi * 2 - math.pi * 0.5
                
                step_position = pg.Vector2(
                    int(w * 0.4 * math.cos(t) + sequence_center[0]),
                    int(w * 0.4 * math.sin(t) + sequence_center[1])
                )
                
                distance = ctx.pointer_position.distance_to(step_position)

                if distance <= step_size * 1.5 and distance < closest:
                    closest = distance
                    step_index = i

        # Based on the current playback position
        if step_index < 0:
            step_index = int(ctx.get_frames_since_last_cycle() / step_length)

        self.loop.toggle_sequence_step(step_index)

    def update(self):
        """ Updates this button from a dirty state """

        super().update()
            
        (x, y, w, h) = self.get_screen_rect()
        margin = ctx.layout.screen_margin_size
        target_size = min(w, h)

        w *= 2
        h *= 2
        margin *= 2

        # Generate waveform
        if not self.loop.sound.any():
            self.icon = None

        else:
            unit = int(ctx.sample_rate / 100)
            self.icon = pg.Surface((w, h))
            self.icon.set_colorkey((0, 0, 0))
            s_w = (w - margin * 2) / (self.loop.get_length() / unit)
            points = []
            offset_frames = self.loop.get_offset_frames()

            for i in range(0, int(self.loop.get_length() / unit)):
                s = i * unit

                # Sometimes the sound length changes between for loop iterations,
                # because it happens on a different thread, so it's not possible
                # to check the length before making this calculation
                try:
                    amplitude = ((self.loop.sound[s][0] + self.loop.sound[s][1]) / 2) / 2

                except:
                    amplitude = 0
                
                s_x = margin + s_w * i
                s_y = h / 2 + amplitude * (h - margin * 2)
                
                points.append((s_x, s_y))

            pg.draw.lines(self.icon, theme.button_text, False, points, 2)
            self.icon = pg.transform.smoothscale(self.icon, (target_size, target_size))
    
        # Generate step sequence
        self.sequence = pg.Surface((w, h))
        self.sequence.set_colorkey((0, 0, 0))
        sequence_length = ctx.beats_per_cycle * 4
        step_length = int((ctx.get_frames_per_beat() * ctx.beats_per_cycle) / (sequence_length))
        sequence_center = (w / 2, h / 2)
        step_shape_width = math.pi * 0.015
        step_size = w * 0.9 / min(64, max(16, sequence_length))
        outer_radius = w * 0.4

        for i in range(0, sequence_length):
            t = (i / sequence_length) * math.pi * 2 - math.pi * 0.5

            # Bars to indicate beats
            if i % 4 == 0:
                beat_line_radius = outer_radius - step_size * 1.5

                beat_point_1 = (
                    beat_line_radius * math.cos(t) + sequence_center[0],
                    beat_line_radius * math.sin(t) + sequence_center[1]
                )
                beat_point_2 = (
                    beat_line_radius * 0.8 * math.cos(t) + sequence_center[0],
                    beat_line_radius * 0.8 * math.sin(t) + sequence_center[1]
                )
            
                pg.draw.line(self.sequence, theme.button_text, beat_point_1, beat_point_2, 4)


            # Circles to indicate steps
            step_circle_pos = (
                outer_radius * math.cos(t) + sequence_center[0],
                outer_radius * math.sin(t) + sequence_center[1]
            )
                
            if self.loop.get_sequence_step(i):
                pg.draw.circle(
                    self.sequence,
                    theme.button_text,
                    step_circle_pos,
                    int(step_size)
                )
            
            else:
                pg.draw.circle(
                    self.sequence,
                    theme.button_text,
                    step_circle_pos,
                    int(step_size / 2)
                )
            
        self.sequence = pg.transform.smoothscale(self.sequence, (target_size, target_size))

    def draw(self):
        """ Draws the button """

        # Hide if this is not the focused loop
        if ctx.focused_loop and ctx.focused_loop != self.loop:
            return

        # Syncronise with loop
        if self.loop.last_update != self.last_loop_update:
            self.is_dirty = True
            self.last_loop_update = self.loop.last_update

        color = theme.button_default
        margin = ctx.layout.screen_margin_size
        (x, y, w, h) = self.get_screen_rect()
        sequence_length = ctx.beats_per_cycle * 4
        step_length = int((ctx.get_frames_per_beat() * ctx.beats_per_cycle) / (sequence_length))

        # Sequence mode
        if ctx.is_sequence_mode():
            if (    # Blink if step is active 
                self.loop.is_playing() and
                self.loop.get_playback_position() > 0 and
                self.loop.get_playback_position() < step_length
            ) or (  # Blink if queued for playback
                not self.loop.is_playing() and
                self.loop.is_queued_for_playing() and
                ctx.blink_state
            ) or (  # Active if pressed
                self.is_pressed() 
            ):
                color = theme.button_active
            
            else:
                color = theme.button_default

        # Recording: Alert
        elif self.loop.is_recording():
            color = theme.button_alert

        # Queued for recording: Blink 2 times per beat
        elif self.loop.is_queued_for_recording():
            if ctx.blink_state:
                color = theme.button_alert

            elif self.loop.is_playing():
                color = theme.button_active

            else:
                color = theme.button_default

        # Queued for playing: Blink 2 times per beat
        elif not self.loop.is_playing() and self.loop.is_queued_for_playing():
            if ctx.blink_state:
                color = theme.button_active

            else:
                color = theme.button_default
        
        # Queued to stop: Blink 2 times per beat
        elif self.loop.is_playing() and self.loop.is_queued_to_stop():
            if ctx.blink_state:
                color = theme.button_active

            else:
                color = theme.button_default

        # Playing: Active
        elif self.loop.is_playing():
            color = theme.button_active

        # Draw the button as specified
        super().draw(color)

        # Sequence mode: Draw the sequence
        if ctx.is_sequence_mode() and self.show_label_until < time.time() and self.loop.sound.any():
            if not self.sequence.get_locked():
                ctx.screen.blit(self.sequence, (x, y))

            # Draw the hand
            sequence_length = ctx.beats_per_cycle * 4
            step_length = int((ctx.get_frames_per_beat() * ctx.beats_per_cycle) / sequence_length)
            step_shape_width = math.pi * 0.01

            t = (ctx.get_current_step() / sequence_length) * math.pi * 2 - math.pi * 0.5
            
            hand_point_1 = (x + w / 2, y + h / 2)
            hand_point_2 = (
                w * 0.3 * math.cos(t) + hand_point_1[0],
                w * 0.3 * math.sin(t) + hand_point_1[1]
            )
            
            pg.draw.line(ctx.screen, theme.button_text, hand_point_1, hand_point_2, 2)

        # Normal mode: Draw the playhead
        elif self.loop.is_playing() or (self.loop.is_recording() and ctx.is_playing()):
            cursor_x = x + margin + (w - margin * 2) * (self.loop.get_playback_position() / self.loop.get_length())
    
            if cursor_x < x + w:
                pg.draw.line(ctx.screen, theme.button_text, (cursor_x, y + margin), (cursor_x, y + margin + h - margin * 2))
            
        # Draw selection
        if ctx.select_rect != None and ctx.select_rect.contains(self.get_window_rect()):
            pg.draw.rect(ctx.screen, theme.button_selected, (x, y, w, h), 1, 4)
   
    def on_double_tapped(self):
        """ Loop button double tapped: Clear the loop """

        if ctx.is_sequence_mode():
            return
        
        self.loop.clear()

    def on_multi_tapped(self):
        """ Loop button multi tapped: Toggle focus """
        
        if self.loop.sound.any():
            if ctx.focused_loop == self.loop:
                ctx.focused_loop = None

            else:
                ctx.focused_loop = self.loop
                
            self.is_dirty = True

    def on_tapped(self):
        """ Loop button tapped: Queue for playback or recording """

        # We want to flip this loop
        if ctx.last_event.type == pg.MOUSEBUTTONUP and ctx.last_event.button == 3:
            self.loop.toggle_reversed()
            return

        # We want to merge this loop with another one
        for button in ctx.get_loop_buttons():
            if button.is_held_down():
                button.loop.absorb_loop_recording(self.loop)
                return
        
        # We want to toggle a step in the sequencer
        if ctx.is_sequence_mode():
            self.toggle_sequence_step()
            return

        # We want to end a recording
        if self.loop.is_recording():
            self.loop.stop_recording()
            self.loop.start_playing()
            return

        # We want to start or stop a recording
        if ctx.is_recording_armed():
            # We want to queue a recording
            if ctx.is_playing():
                # We want to cancel recording
                if self.loop.is_queued_for_recording():
                    self.loop.cancel_queued_state()

                # We want to queue the loop for recording
                else:
                    self.loop.queue_recording()

            # We want to record without a tempo
            else:
                self.loop.start_recording()

            return
       
        # We want to queue the loop for playing
        self.loop.toggle_play()
   
    def get_label(self) -> str:
        """ Shows the label depicting the offset or volume """
        
        # Offset
        if self.loop.last_changed_value == 'offset':
            if self.loop.offset < 0:
                return str(self.loop.offset) + ' ms'
            
            if self.loop.offset > 0:
                return '+' + str(self.loop.offset) + ' ms'
            
            return '0 ms'

        # Speed
        if self.loop.last_changed_value == 'speed':
            return str(round(self.loop.speed, 2)) + 'x'

        # Volume
        return str(self.loop.volume) + '%'
    
    def get_icon_alpha(self) -> float:
        if ctx.is_sequence_mode():
            return 50

        return super().get_icon_alpha()

    def get_screen_rect(self) -> pg.Rect:
        """ If this loop is focused, maximise it """

        if ctx.focused_loop != self.loop:
            return super().get_screen_rect()

        # Calculate space taken up by other loops and use it
        min_x = 10000
        min_y = 10000
        max_x = 0
        max_y = 0

        for button in ctx.get_loop_buttons():
            if button == self:
                continue

            rect = button.get_screen_rect()

            min_x = min(min_x, rect[0])
            min_y = min(min_y, rect[1])
            max_x = max(max_x, rect[0] + rect[2])
            max_y = max(max_y, rect[1] + rect[3])

        return pg.Rect(
           min_x,
           min_y,
           max_x - min_x,
           max_y - min_y
        )

    def is_disabled(self) -> bool:
        """ If a loop is in focus, and it's not this one, disable this button """

        if ctx.focused_loop and ctx.focused_loop != self.loop:
            return True

        return super().is_disabled()

    def on_slide_x(self, x, dx, mx):
        """ Pointer slid across this button horizontally: Change offset """

        if not self.loop.sound.any() or ctx.is_recording_armed():
            return

        self.loop.change_offset(dx)
        self.show_label()    
    
    def on_slide_y(self, y, dy, my):
        """ Pointer(s) slid across this button vertically: Change volume or speed """

        (bx, by, bw, bh) = self.get_screen_rect()

        slide_interval = int(bh / 10)

        if my % slide_interval != 0:
            return

        if len(self.pressed_pointers) > 1 or (ctx.last_event.type == pg.MOUSEBUTTONDOWN and ctx.last_event.button == 3):
            self.loop.change_speed(-dy)

        else:
            self.loop.change_volume(-dy)

        self.show_label()    

    def on_selection(self, others):
        """ A selection of this and other loops was made """

        for button in others:
            self.loop.absorb_loop_recording(button.loop)

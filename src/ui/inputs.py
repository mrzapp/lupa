"""
This module handles all user input
"""

from enum import Enum
import time
import typing

import pygame as pg

from ..context import ctx

class LupaInputSlideAxis(Enum):
    NONE = 0
    X = 1
    Y = 2

class LupaInputs:
    pointer_down_pos = None
    slide_axis = LupaInputSlideAxis.NONE
    events = []
    next_key_repeat = 0

    def process(self):
        """ Handles input events """
      
        self.events = pg.event.get()

        # Input events
        for event in self.events:
            if event.type == pg.FINGERDOWN or event.type == pg.MOUSEBUTTONDOWN:
                self.on_pointer_down(event)
            
            elif event.type == pg.FINGERUP or event.type == pg.MOUSEBUTTONUP:
                self.on_pointer_up(event)
           
            elif event.type == pg.MOUSEMOTION or event.type == pg.FINGERMOTION:
                self.on_pointer_motion(event)

            elif event.type == pg.KEYDOWN:
                self.on_key_down(event)
                
            elif event.type == pg.KEYUP:
                self.on_key_up(event)

        # Key repeat
        if time.time() >= self.next_key_repeat:
            self.next_key_repeat = time.time() + 0.05

            for button in ctx.get_all_buttons():
                if not button.can_key_repeat or not button.is_held_down():
                    continue

                button.on_key_repeat()

    def on_pointer_down(self, event):
        """ Finger or mouse button down """

        ctx.last_event = event

        if event.type == pg.FINGERDOWN:
            w, h = ctx.window.get_size()
            x = event.x * w
            y = event.y * h
            identifier = event.finger_id

        elif event.type == pg.MOUSEBUTTONDOWN:
            (x, y) = event.pos
            identifier = event.button

            # We only accept left/right click
            if identifier != 1 and identifier != 3:
                return

        ctx.pointer_position.x = x
        ctx.pointer_position.y = y

        self.pointer_down_pos = pg.Vector2(x, y)
        self.slide_axis = LupaInputSlideAxis.NONE

        # We're interacting with a button
        for button in ctx.get_all_buttons():
            if button.is_disabled():
                continue

            if button.get_window_rect().collidepoint(x, y):
                button.on_pressed(identifier)
                return

        # We're drawing a selection rectangle
        if not ctx.is_recording_armed():
            ctx.select_rect = pg.Rect(x, y, 0, 0)

    def on_pointer_up(self, event):
        """ Finger or mouse button up """
            
        ctx.last_event = event

        if event.type == pg.FINGERUP:
            w, h = ctx.window.get_size()
            x = event.x * w
            y = event.y * h
            identifier = event.finger_id

        elif event.type == pg.MOUSEBUTTONUP:
            (x, y) = event.pos
            identifier = event.button
        
        ctx.pointer_position.x = x
        ctx.pointer_position.y = y
       
        # Release buttons
        for button in ctx.get_all_buttons():
            if button.is_pressed(identifier):
                button.on_released(identifier)
        
        # Loop multiple selection: Find a primary loop and secondary loops
        if ctx.select_rect != None:
            primary_selected_loop = None
            secondary_selected_loops = []

            for loop in ctx.get_loop_buttons():
                rect = loop.get_window_rect()

                if ctx.select_rect.contains(rect):
                    secondary_selected_loops.append(loop)
                    
                    if not primary_selected_loop:
                        primary_selected_loop = loop
                    
                    # The primary loop is the one closest to the original cursor position
                    if loop.get_window_pos().distance_to(self.pointer_down_pos) < primary_selected_loop.get_window_pos().distance_to(self.pointer_down_pos):
                        primary_selected_loop = loop

            # Execute selection event on primary loop
            if primary_selected_loop and len(secondary_selected_loops) > 0:
                secondary_selected_loops.remove(primary_selected_loop)
                
                primary_selected_loop.on_selection(secondary_selected_loops)

        # Reset slide and pointer variables
        self.pointer_down_pos = None
        self.slide_axis = LupaInputSlideAxis.NONE

        # Reset select rectangle
        ctx.select_rect = None

    def on_pointer_motion(self, event):
        """ Finger or mouse moved """

        if not self.pointer_down_pos:
            return

        if event.type == pg.MOUSEMOTION:
            (x, y) = event.pos
            (dx, dy) = event.rel
            identifier = 1

            # Rather than just a simple list of identifiers, event.buttons
            # is a strange tuple where each positional element represents
            # an on/off state, for example a left mouse button: (1, 0, 0)
            for i in range(0, len(event.buttons)):
                if event.buttons[i] == 1:
                    identifier = i + 1
                    break

        elif event.type == pg.FINGERMOTION:
            w, h = ctx.get_screen_size()
            x = event.x * w
            y = event.y * h
            dx = event.dx
            dy = event.dy
            identifier = event.finger_id
        
        ctx.pointer_position.x = x
        ctx.pointer_position.y = y
        
        mx = x - self.pointer_down_pos[0]
        my = y - self.pointer_down_pos[1]

        # Lock slide axis down before propagating slide events
        if self.slide_axis == LupaInputSlideAxis.NONE:
            if abs(mx) > 10:
                self.slide_axis = LupaInputSlideAxis.X

            elif abs(my) > 10:
                self.slide_axis = LupaInputSlideAxis.Y

        for button in ctx.get_all_buttons():
            if button.is_disabled():
                continue

            if button.is_pressed(identifier):
                if self.slide_axis == LupaInputSlideAxis.X:
                    button.on_slide_x(x, dx, mx)

                elif self.slide_axis == LupaInputSlideAxis.Y:
                    button.on_slide_y(y, dy, my)

        # Select rectangle
        if ctx.is_recording_armed():
            ctx.select_rect = None

        if ctx.select_rect != None:
            if mx < 0:
                ctx.select_rect[0] = self.pointer_down_pos[0] + mx
                ctx.select_rect[2] = self.pointer_down_pos[0] - ctx.select_rect[0]

            else:
                ctx.select_rect[0] = self.pointer_down_pos[0]
                ctx.select_rect[2] = mx
            
            if my < 0:
                ctx.select_rect[1] = self.pointer_down_pos[1] + my
                ctx.select_rect[3] = self.pointer_down_pos[1] - ctx.select_rect[1]

            else:
                ctx.select_rect[1] = self.pointer_down_pos[1]
                ctx.select_rect[3] = my

    def on_key_down(self, event):
        """ Key was pressed """

        ctx.last_event = event

        for button in ctx.get_all_buttons():
            if button.key != event.key or button.is_disabled():
                continue

            button.on_pressed(event.key)

    def on_key_up(self, event):
        """ Key was released """

        ctx.last_event = event

        for button in ctx.get_all_buttons():
            if button.key != event.key or button.is_disabled():
                continue

            button.on_released(event.key)

inputs = LupaInputs()

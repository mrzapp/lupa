from .buttons import LupaToggleButton

class LupaMetronomeButton(LupaToggleButton):
    """ The button responsible for playing the metronome sounds """
    
    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'metronome')

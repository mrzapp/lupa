import os
import time
import threading
from glob import glob

import pygame as pg
import numpy as np

from .theme import theme
from ..context import ctx

class LupaButtonEvent:
    pointer = -1
    time = 0

    def __init__(self, pointer, time):
        self.pointer = pointer
        self.time = time

    def set(self, pointer, time):
        self.pointer = pointer
        self.time = time

class LupaButton:
    can_key_repeat = False
    icon = None
    icon_name = ''
    icon_source = None
    is_dirty = True
    key = -1
    label = ''
    last_pressed = None
    last_released = None
    last_tapped = None
    last_update = 0
    pressed_pointers = None
    rect = (0, 0, 0, 0)
    show_label_until = 0
    update_thread = None
    
    def __init__(self, key, rect, icon = ''):
        """ Constructor """

        self.key = key
        self.rect = rect
        self.pressed_pointers = []
        self.icon_name = icon
        self.last_pressed = LupaButtonEvent(-1, 0)
        self.last_released = LupaButtonEvent(-2, 0)
        self.last_tapped = LupaButtonEvent(-3, 0)

    def show_label(self, new_label = ''):
        self.show_label_until = time.time() + 1.0

        if new_label:
            self.label = str(new_label)

    def is_disabled(self) -> bool:
        return False

    def get_label(self) -> str:
        return self.label
    
    def get_icon_alpha(self) -> float:
        if self.show_label_until > time.time():
            return 50

        return 255

    def get_screen_pos(self) -> pg.Vector2:
        rect = self.get_screen_rect()
        return pg.Vector2(rect[0] + rect[2] / 2, rect[1] + rect[3] / 2)
    
    def get_window_pos(self) -> pg.Vector2:
        rect = self.get_window_rect()
        return pg.Vector2(rect[0] + rect[2] / 2, rect[1] + rect[3] / 2)

    def get_screen_rect(self) -> pg.Rect:
        """ Gets the drawing rectangle multiplied by the layout's size definitions """

        (x, y, w, h) = self.rect

        margin = ctx.layout.screen_margin_size
        grid_w = ctx.layout.screen_grid_width
        grid_h = ctx.layout.screen_grid_height

        button_width = ctx.screen.get_width() / grid_w - margin / grid_w
        button_height = ctx.screen.get_height() / grid_h - margin / grid_h

        w = w * button_width - margin
        h = h * button_height - margin
        x = margin + x * button_width
        y = margin + y * button_height

        return pg.Rect(int(x), int(y), int(w), int(h))

    def get_window_rect(self) -> pg.Rect:
        """ Gets the interaction rectangle """

        rect = self.get_screen_rect()
        x = ctx.window.get_width() / 2 - ctx.screen.get_width() / 2
        y = ctx.window.get_height() / 2 - ctx.screen.get_height() / 2

        rect.x += x
        rect.y += y

        return rect

    def on_slide(self, x, y, dx, dy, mx, my):
        """ Pointer slid across button """
    
    def on_slide_x(self, x, dx, mx):
        """ Pointer slid across button on the horizontally """
    
    def on_slide_y(self, y, dy, my):
        """ Pointer slid across button on the vertically """
    
    def on_swiped_x(self, mx):
        """ Pointer was released after sliding across button on the horizontally """
    
    def on_swiped_y(self, my):
        """ Pointer was released after sliding across button on the vertically """

    def on_pressed(self, pointer):
        """ Button pressed """
      
        if not pointer in self.pressed_pointers:
            self.pressed_pointers.append(pointer)

        self.last_pressed.set(pointer, time.time())

    def on_released(self, pointer):
        """ Button released """
       
        was_tapped = pointer == self.last_pressed.pointer and time.time() - self.last_pressed.time < 0.2

        if was_tapped:
            if time.time() - self.last_tapped.time < 0.2 and self.last_released.pointer == pointer:
                self.on_double_tapped()
            
            elif len(self.pressed_pointers) > 1:
                self.on_multi_tapped()

            else:
                self.on_tapped()
            
            self.last_tapped.set(pointer, time.time())
     
        if pointer in self.pressed_pointers:
            self.pressed_pointers.remove(pointer)

        self.last_released.set(pointer, time.time())

    def is_pressed(self, pointer = -1) -> bool:
        """ Check if button is currently pressed """

        if pointer > -1:
            return pointer in self.pressed_pointers

        return len(self.pressed_pointers) > 0
   
    def is_held_down(self) -> bool:
        """ Check if button has been pressed for more than 0.2 seconds """

        return self.is_pressed() and time.time() - self.last_pressed.time >= 0.2

    def on_released_after_hold(self):
        """ Button released after being held down for > 0.2 seconds """

    def on_tapped(self):
        """ Button tapped (pressed and released quickly) """
    
    def on_multi_tapped(self):
        """ Button tapped again with another pointer (pressed and released quickly) """
    
    def on_double_tapped(self):
        """ Button tapped again (pressed and released quickly) """
    
    def on_key_repeat(self):
        """ Button held down using key repeat """

    def draw(self, color = theme.button_default):
        """ Draws the button """
       
        margin = ctx.layout.screen_margin_size
        rect = self.get_screen_rect()
        (x, y, w, h) = rect
    
        if self.is_pressed():
            color = theme.button_active
        
        pg.draw.rect(ctx.screen, color, rect, 0, 4)
        
        # Draw the icon
        if self.icon and not self.icon.get_locked():
            self.icon.set_alpha(self.get_icon_alpha())

            target_x = x + w / 2 - self.icon.get_width() / 2
            target_y = y + h / 2 - self.icon.get_height() / 2
            
            ctx.screen.blit(self.icon, (target_x, target_y))

        # Draw the label
        if self.show_label_until > time.time():
            (tw, th) = ctx.font.size(self.get_label())
            
            ctx.screen.blit(ctx.font.render(self.get_label(), True, theme.button_text), (x + w / 2 - tw / 2, y + h - th - margin))

    def update_async(self):
        """ Calls the update method in a separate thread """

        self.update_thread = threading.Thread(target = self.update)
        self.update_thread.start()
        self.is_dirty = False

    def update(self):
        """ Updates this button from a dirty state """
        
        # Load icon
        if self.icon_name != '' and not self.icon_source:
            self.icon_source = pg.image.load(ctx.path('assets/images/' + self.icon_name + '.svg')).convert_alpha()

        # Resize icon
        if self.icon_source:
            (button_x, button_y, button_width, button_height) = self.get_screen_rect()
            
            target_size = min(int(button_width / 2), int(button_height / 2))
            target_x = button_x + button_width / 2 - target_size / 2
            target_y = button_y + button_height / 2 - target_size / 2

            self.icon = pg.transform.smoothscale(self.icon_source, (target_size, target_size))

        self.last_update = time.time()
        self.is_dirty = False

class LupaToggleButton(LupaButton):
    """ A button with a toggle state """

    is_active = False
    
    def __init__(self, key, rect, icon = ''):
        """ Constructor """

        super().__init__(key, rect, icon)

        self.update_active_state_from_keyboard()

    def update_active_state_from_keyboard(self):
        """ If the associated physical key is a toggle, sync up with its state """

        if self.key == pg.K_NUMLOCK:
            for file_path in glob('/sys/class/leds/input*numlock/brightness'):
                file = open(file_path, 'r')
                brightness = int(file.read().splitlines()[0])

                self.is_active = brightness > 0
        
        elif self.key == pg.K_CAPSLOCK:
            for file_path in glob('/sys/class/leds/input*capslock/brightness'):
                file = open(file_path, 'r')
                brightness = int(file.read().splitlines()[0])

                self.is_active = brightness > 0

    def draw(self, color = None):
        """ Draws the toggle button """

        if not color:
            color = theme.button_default

            if self.is_active or self.is_pressed():
                color = theme.button_active

        super().draw(color)

    def on_released(self, identifier):
        """ Toggle button released """

        super().on_released(identifier)

        self.update_active_state_from_keyboard()

    def on_tapped(self):
        """ Switch state on tap """

        self.is_active = not self.is_active
        
class LupaNumericChangeButton(LupaButton):
    """ Changes numeric values depending on which other button is pressed """

    def __init__(self, key, rect, icon = ''):
        """ Constructor """

        self.can_key_repeat = True

        super().__init__(key, rect, icon)

    def change_value(self, direction):
        """ Change a value """
        
        # Loop buttons
        for button in ctx.get_loop_buttons():
            if button.is_pressed():
                # Loop offset
                if ctx.layout.cycle_button.last_pressed.time > button.last_pressed.time and ctx.layout.cycle_button.last_pressed.time > ctx.layout.tempo_button.last_pressed.time:
                    button.loop.change_offset(direction)
                    button.show_label()
                
                # Loop speed
                elif ctx.layout.tempo_button.last_pressed.time > button.last_pressed.time and ctx.layout.tempo_button.last_pressed.time > ctx.layout.cycle_button.last_pressed.time:
                    button.loop.change_speed(direction)
                    button.show_label()

                # Loop volume
                else:
                    button.loop.change_volume(direction)
                    button.show_label()

                return

        # Tempo
        if ctx.layout.tempo_button.is_pressed():
            ctx.layout.tempo_button.change_tempo(direction)
            return

        # Cycles
        if ctx.layout.cycle_button.is_pressed():
            ctx.layout.cycle_button.change_cycle(direction)
            return
    
        # Input delay
        if ctx.layout.record_button.is_pressed():
            ctx.layout.record_button.change_input_delay(direction)
            return

from .buttons import LupaNumericChangeButton
from ..context import ctx

class LupaIncreaseButton(LupaNumericChangeButton):
    """ The button for increasing values """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'caret-up')
    
    def on_tapped(self):
        """ Increase value on tap """

        self.change_value(1)
    
    def on_double_tapped(self):
        """ Increase value on tap """

        self.change_value(1)
    
    def on_key_repeat(self):
        """ Increase value on key repeat """

        self.change_value(1)

from .buttons import LupaToggleButton
from .theme import theme
from ..context import ctx

class LupaRecordButton(LupaToggleButton):
    """ The button for recording new loops """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'microphone')
   
    def is_disabled(self) -> bool:
        return ctx.is_sequence_mode()

    def get_label(self) -> str:
        return str(ctx.input_delay) + ' ms'

    def draw(self):
        """ Draws this button """

        color = theme.button_default

        if self.is_active or self.is_pressed():
            color = theme.button_alert

        super().draw(color)

    def on_tapped(self):
        """ On tap """

        super().on_tapped()

        # Stop all active recordings
        for loop in ctx.loops:
            if not self.is_active and loop.is_recording():
                loop.stop_recording()
                loop.start_playing()
            
    def on_slide_y(self, y, dy, my):
        """ Pointer slid across this button vertically: Change input delay """
        
        (bx, by, bw, bh) = self.get_screen_rect()

        if my % int(bh / 4) == 0:
            self.change_input_delay(-dy)

    def change_input_delay(self, direction):
        """ Change the input delay by 10s of milliseconds """
        ctx.input_delay = max(0, int(ctx.input_delay + direction))
        self.show_label()

import time

from .buttons import LupaButton
from .theme import theme
from ..context import ctx

class LupaTempoButton(LupaButton):
    """ The button for changing the tempo """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'clock')
   
    def get_label(self) -> str:
        return str(ctx.beats_per_minute) + ' BPM'
    
    def on_tapped(self):
        """ Listens for taps to let the user tap the desired tempo """

        # Cancel if a loop button is pressed
        for button in ctx.get_loop_buttons():
            if button.is_pressed():
                return

        # Set BPM
        if self.last_tapped.time > 0 and time.time() - self.last_tapped.time > 0:
            # BPM allowed range is 20 - 200, and only multiples of 2
            bpm = (int(min(200, 60 / (time.time() - self.last_tapped.time))) // 2) * 2

            # If the BPM was set < 20, it probably was not intended as a tempo tap
            if bpm < 20:
                return

            ctx.beats_per_minute = bpm 
            self.show_label()

        super().on_tapped()

    def draw(self):
        """ Draws the tempo button, highlighting it every beat """

        color = theme.button_default

        if (ctx.is_playing() and ctx.get_time_since_last_beat() < 0.1) or self.is_pressed():
            color = theme.button_active

        super().draw(color)
    
    def on_slide_y(self, y, dy, my):
        """ Pointer slid across this button vertically: Change tempo """
        
        (bx, by, bw, bh) = self.get_screen_rect()

        if my % int(bh / 4) == 0:
            self.change_tempo(-dy)

    def change_tempo(self, direction):
        """ Change the beats per minute """

        ctx.beats_per_minute = max(20, min(200, ctx.beats_per_minute + 2 * direction))
        self.show_label()

        for loop in ctx.loops:
            loop.is_dirty = True

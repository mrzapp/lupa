from .buttons import LupaNumericChangeButton
from ..context import ctx

class LupaDecreaseButton(LupaNumericChangeButton):
    """ The button for decreasing values """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'caret-down')
    
    def on_tapped(self):
        """ Decrease value on tap """

        self.change_value(-1)
    
    def on_double_tapped(self):
        """ Decrease value on tap """

        self.change_value(-1)
    
    def on_key_repeat(self):
        """ Decrease value on key repeat """

        self.change_value(-1)

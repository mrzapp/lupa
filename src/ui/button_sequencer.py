from .buttons import LupaToggleButton
from ..context import ctx

class LupaSequencerButton(LupaToggleButton):
    """ The button for switching between sequencer and loop mode """

    def __init__(self, p_key, p_rect):
        """ Constructor """

        super().__init__(p_key, p_rect, 'sequencer')

    def on_tapped(self):
        """ Make sure the record button is off """

        if ctx.layout.record_button.is_active:
            ctx.layout.record_button.on_tapped()

        # If a loop is held down, reset its step sequence
        for button in ctx.get_loop_buttons():
            if button.is_pressed():
                button.loop.reset_sequence()
                return

        super().on_tapped()

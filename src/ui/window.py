"""
This is where everything related to the GUI is handled
"""

import os
import time

import pygame as pg

from ..config import cfg
from ..context import ctx
from .layout import LupaLayout
from .theme import theme
from .inputs import inputs

class LupaWindow:
    last_draw_call = 0

    def init(self):
        """ Constructor """
        
        pg.init()
        pg.display.set_caption('Lupa')
       
        layout_name = 'layout.' + cfg['lupa']['layout']

        if not layout_name in cfg:
            raise Exception('Layout "' + layout_name + '" not found')

        ctx.layout = LupaLayout(cfg[layout_name])

        icon = pg.image.load(ctx.path('icon.svg')) 
        icon = pg.transform.smoothscale(icon, (64, 64))
        pg.display.set_icon(icon)

        ctx.window = pg.display.set_mode(ctx.get_screen_size(), pg.HWSURFACE|pg.DOUBLEBUF|pg.RESIZABLE) 
        ctx.screen = pg.Surface(ctx.get_screen_size()).convert_alpha() 

        self.on_resize()

    def on_resize(self):
        """ Screen was resized """
        
        # Fix aspect ratio
        original_size = ctx.get_screen_size()
        window_size = pg.Vector2(ctx.window.get_size())
        screen_size = pg.Vector2(ctx.screen.get_size())
        
        screen_size.x = window_size.x
        screen_size.y = window_size.x * (original_size.y / original_size.x)

        if screen_size.y > window_size.y:
            screen_size.y = window_size.y
            screen_size.x = window_size.y * (original_size.x / original_size.y)

        ctx.screen = pg.Surface(screen_size).convert_alpha() 
        
        # Resize font
        unit_h = int(ctx.screen.get_height() / ctx.layout.screen_grid_height)
        unit_w = int(ctx.screen.get_width() / ctx.layout.screen_grid_width)
        font_size = int(min(unit_h, unit_w) / 6)
        ctx.font = pg.font.Font(ctx.path('assets/fonts/cantarell.otf'), font_size)

        # Mark buttons
        for button in ctx.get_all_buttons():
            button.is_dirty = True

    def layout_to_screen_rect(self, rect) -> pg.Rect:
        """ Converts a layout rect to a screen rect """

        margin = ctx.layout.screen_margin_size
        grid_w = ctx.layout.screen_grid_width
        grid_h = ctx.layout.screen_grid_height
        screen_unit_w = (ctx.screen.get_width() / grid_w - margin / grid_w)
        screen_unit_h = (ctx.screen.get_height() / grid_h - margin / grid_h)
    
        return pg.Rect(
            screen_unit_w * rect[0],
            screen_unit_h * rect[1],
            screen_unit_w * rect[2],
            screen_unit_h * rect[3]
        )

    def draw(self):
        """ Draws the screen """
        
        # Clear window and screen
        ctx.screen.fill(theme.background)
        ctx.window.fill(theme.background)
        
        # Draw buttons
        for button in ctx.get_all_buttons():
            if button.is_dirty:
                button.update_async()
            
            button.draw()
        
        # Draw log string
        if ctx.log_string != '':
            pg.draw.rect(ctx.screen, theme.button_default, (0, 0, ctx.screen.get_width(), 40))
            ctx.screen.blit(ctx.font.render(ctx.log_string, True, theme.button_text), (10, 10))

        # Draw screen
        ctx.window.blit(ctx.screen, (ctx.window.get_width() / 2 - ctx.screen.get_width() / 2, ctx.window.get_height() / 2 - ctx.screen.get_height() / 2))

        # Draw selection box
        if ctx.select_rect != None:
            pg.draw.rect(ctx.window, theme.button_selected, ctx.select_rect, 1)
       
        pg.display.flip()

    def tick(self):
        """ The UI main loop """
      
        inputs.process()
       
        # Draw at roughly 30 FPS
        if time.time() - self.last_draw_call >= 1 / 30:
            self.last_draw_call = time.time()
            self.draw()

        # Listen for resize
        for event in inputs.events:
            if event.type == pg.VIDEORESIZE:
                self.on_resize()
                break
            
            elif event.type == pg.QUIT:
                ctx.is_running = False
                break

window = LupaWindow()

# Lupa

![Lupa logo](assets/images/title.svg)

Lupa is a simple open-source audio looper and step sequencer for Linux. Built for monkeying around with music.


![Screenshot](screenshot.jpg)


## Contents

[TOC]


## Downloads

Latest binary release available [here](https://mrzapp.gitlab.io/lupa/lupa-x86_64)

Source code available [here](https://gitlab.com/mrzapp/lupa/)


## Features

- 9 loops
- Loop offset
- Loop speed
- Loop reversal
- Loop merging
- Loop overdubbing
- Step sequencer
- Output buses
- Metronome
- Cycle repeat
- Input delay compensation


## Requirements

- JACK Audio Connection Kit
- Python libraries (when running from source):
    - [JACK-Client](https://pypi.org/project/JACK-Client/)
    - [numpy](https://pypi.org/project/numpy/)
    - [pygame](https://pypi.org/project/pygame/)
    - [soundfile](https://pypi.org/project/soundfile/)


## Common questions

### Will there be support for Windows/MacOS/FreeBSD/Haiku/GNU Hurd?

Not officially, but if anyone is willing to maintain non-Linux builds, they will be linked to in the downloads section.


### I am not able to use certain key combinations, what gives?

This is a hardware limitation on some keyboards that don't support n-key rollover. You can try changing layouts, see below.


## Layouts

Lupa ships with 3 layouts built-in:  

- **touch**: For mouse and touch
- **numpad**: Recommended key-based layout
- **keyboard**: For keyboards without numpad

You can also create your own layouts (see "[Configuration](#configuration)" below).


## Configuration

Config file location: `~/.config/lupa/lupa.cfg`

You can find the default config file [here](./lupa.cfg)

### General
- `layout`: The current UI layout. Must correspond to one listed below.
- `input_delay`: The default input delay. Can be changed while Lupa is running.
- `transport`: Lupa's role as a JACK client. It can either be set to `lead`, `follow` or `none`.
- `buses`: Audio output buses. If none are defined, all audio is mixed into one.
    - Buses are specified as `name|loops`
    - `loops` is a comma-separated list of numbers from 1 to 9.
    - The `name|` part is optional.


### Layouts
- The section heading is formatted as `[layout.mylayout]`.
- `screen_width/height`: Defines the default size of the window.
- `screen_grid_width/height`: Divides the window into a grid.
- `screen_margin_size`: The space between the buttons.
- `*_button`: The interactable UI buttons.
    - Buttons are specified as `key|x,y,width,height`.
    - The `key|` part is optional, not needed for touchscreens.


## Buttons (keyboard, touch or mouse)

| Buttons                                           | Effect                                            |
| ------------------------------------------------- | ------------------------------------------------- |
| Tap `METRONOME`                                   | Toggle metronome on/off                           |
| Tap `TEMPO`                                       | Tap tempo                                         |
| Hold `TEMPO` and tap `UP`/`DOWN`                  | Increase/decrease tempo                           |
| Hold `CYCLE` and tap `UP`/`DOWN`                  | Increase/decrease beats per cycle                 |
| Tap `SEQUENCER`                                   | Toggle step sequencer mode on/off                 |
| Tap `NUMBER`                                      | Toggle loop playback/record on/off                |
| Tap `NUMBER` in sequencer                         | Toggle sequence step at current position          |
| Double tap `NUMBER`                               | Delete loop recording                             |
| Hold `NUMBER` and tap another `NUMBER`            | Merge loops                                       |
| Hold `NUMBER` and tap `UP`/`DOWN`                 | Increase/decrease loop volume                     |
| Hold `NUMBER`, tap `TEMPO` and tap `UP`/`DOWN`    | Increase/decrease loop speed                      |
| Hold `NUMBER`, tap `CYCLE` and tap `UP`/`DOWN`    | Increase/decrease loop offset                     |
| Hold `NUMBER` and tap `PLAYBACK`                  | Toggle loop reversal                              |
| Hold `NUMBER` and tap `SEQUENCER`                 | Clear loop step sequence                          |
| Tap `RECORD`                                      | Toggle recording on/off                           |
| Hold `RECORD` and tap `NUMBER`                    | Queue loop recording                              |
| Hold `RECORD` and tap `UP`/`DOWN`                 | Increase/decrease input delay                     |
| Tap `PLAYBACK`                                    | Toggle playback on/off                            |
| Hold `PLAYBACK`                                   | Repeat cycle every beat                           |


## Gestures (touch or mouse)

**NOTE:** 2 finger gestures with the mouse means clicking both left and right button

| Gesture                                           | Effect                                            |
| ------------------------------------------------- | ------------------------------------------------- |
| Slide `TEMPO` up/down                             | Increase/decrease tempo                           |
| Slide `CYCLE` up/down                             | Increase/decrease beats per cycle                 |
| Drag selection around multiple `NUMBER`           | Merge loops                                       |
| Slide `NUMBER` left/right                         | Increase/decrease loop offset                     |
| Slide `NUMBER` up/down                            | Increase/decrease loop volume                     |
| Slide `NUMBER` up/down with 2 fingers             | Increase/decrease loop speed                      |
| Slide `NUMBER` up/down with right mouse           | Increase/decrease loop speed                      |
| Hold `NUMBER` and tap with another finger         | Maximise loop, useful for step sequencing         |
| Right click `NUMBER`                              | Toggle loop reversal                              |
| Slide `RECORD` up/down                            | Increase/decrease input delay                     |


## Recording

Recordings are always overdubbed. There are 2 ways to start them:

**Record without tempo**: When `RECORD` is active, `PLAYBACK` is **not** active and `NUMBER` is tapped, the recording is started immediately. When the recording is stopped, a new tempo is determined based on the length of the recording.

**Queue a recording**: When `RECORD` and `PLAYBACK` are both active and `NUMBER` is tapped, the recording is queued for the next cycle.

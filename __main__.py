__version__ = '0.11.7'

import argparse
import jack
import os

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog = 'Lupa', description = 'A multi-layout audio looper')
    parser.add_argument('-v', '--version', action = 'store_true', help = 'Display version number')
    args = parser.parse_args()

    if args.version:
        print(__version__)

    else:
        from src.engine import engine
        
        engine.init()
